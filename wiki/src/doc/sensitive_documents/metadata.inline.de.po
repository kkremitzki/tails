# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"POT-Creation-Date: 2021-06-10 19:12-0500\n"
"PO-Revision-Date: 2021-10-30 13:04+0000\n"
"Last-Translator: Weblate Admin <tails-weblate@boum.org>\n"
"Language-Team: Tails translators <tails@boum.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta robots=\"noindex\"]]\n"
msgstr "[[!meta robots=\"noindex\"]]\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "<p>Most files contain metadata which is information characterizing the\n"
#| "content of the file. For example:</p>\n"
msgid ""
"<p>Most files contain metadata, which is information used to describe,\n"
"identify, categorize, and sort files. For example:</p>\n"
msgstr ""
"<p>Die meisten Dateien enthalten Metadaten, was Informationen sind, die \n"
"den Inhalt der Datei kennzeichnen. Zum Beispiel:</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<ul>\n"
"<li>Cameras record data about when and where a picture was taken and what\n"
"camera was used.</li>\n"
"<li>Office documents automatically add author\n"
"and company information to texts and spreadsheets.</li>\n"
"</ul>\n"
msgstr ""
"<ul>\n"
"<li>Kameras zeichnen Daten darüber auf, wann und wo ein Foto gemacht wurde und welche\n"
"Kamera benutzt wurde.</li>\n"
"<li>Dokumente fügen automatisch Autoren- und\n"
"Firmeninformationen zu Texten und Tabellenkalkulationen hinzu.</li>\n"
"</ul>\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "<p>You can use the <span\n"
#| "class=\"application\"><a href=\"https://0xacab.org/jvoisin/mat2\">MAT</a></span> to\n"
#| "clean the metadata from your files before publishing them.</p>\n"
msgid ""
"<p>Use [[<i>mat2</i>|doc/sensitive_documents/metadata]] to\n"
"clean metadata from your files before publishing or sharing them:</p>\n"
msgstr ""
"<p>Sie können <span\n"
"class=\"application\"><a href=\"https://0xacab.org/jvoisin/mat2\">MAT</a></span> benutzen, um\n"
"die Metadaten von Ihren Dateien zu bereinigen, bevor Sie diese veröffentlichen.</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<ol>\n"
"<li>Open the <span class=\"application\">Files</span> browser.</li>\n"
"<li>Navigate to the folder containing the files that you want to remove\n"
"   metadata from.</li>\n"
"<li>Select the files that you want to remove metadata from.</li>\n"
"<li>Right-click (on Mac, click with two fingers) on the files and choose\n"
"<strong>Remove metadata</strong>.</li>\n"
"</ol>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"
