[[!meta title="Troubleshooting connecting to Tor"]]

The sections below summarize the most common problems when connecting to Tor.

The computer clock is set to an incorrect time zone
---------------------------------------------------

Your clock and time zone need to be correct to connect to Tor.

Choose **Fix Clock** in the error screen of the *Tor Connection* assistant to
set the clock and time zone of your computer.

You did not enter the bridge correctly
--------------------------------------

Only obfs4 bridges can be used in Tails currently.

An obfs4 bridge looks like:

    obfs4 1.2.3.4:1234 B0E566C9031657EA7ED3FC9D248E8AC4F37635A4 cert=OYWq67L7MDApdJCctUAF7rX8LHvMxvIBPHOoAp0+YXzlQdsxhw6EapaMNwbbGICkpY8CPQ iat-mode=0

You need to enter the entire line, not just the IP address and port
combination.

The bridge is no longer operational
-----------------------------------

It is possible that the bridge that you entered is no longer working.

Try entering another bridge or requesting other bridges.
